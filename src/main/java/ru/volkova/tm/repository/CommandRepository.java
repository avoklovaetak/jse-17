package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    /*@Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>;
        for (final AbstractCommand command: commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getArgumentNames() {
        final List<String> result = new ArrayList<>;
        for (final AbstractCommand command: commands.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }*/

    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    public AbstractCommand getCommandByArg(String name) {
        return commands.get(name);
    }

}
