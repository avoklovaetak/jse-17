package ru.volkova.tm.bootstrap;

import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.*;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.command.bonds.ProjectCascadeRemoveByIdCommand;
import ru.volkova.tm.command.bonds.TaskBindByProjectIdCommand;
import ru.volkova.tm.command.bonds.TaskUnbindByProjectId;
import ru.volkova.tm.command.bonds.TasksShowByProjectIdCommand;
import ru.volkova.tm.command.project.*;
import ru.volkova.tm.command.system.*;
import ru.volkova.tm.command.task.*;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.repository.CommandRepository;
import ru.volkova.tm.repository.ProjectRepository;
import ru.volkova.tm.repository.TaskRepository;
import ru.volkova.tm.service.*;
import ru.volkova.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    public void initData() {
        projectService.add("DEMO 1", "1").setStatus(Status.NOT_STARTED);
        projectService.add("DEMO 4", "1").setStatus(Status.NOT_STARTED);
    }

    public void run(final String... args){
        logService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            logService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                logService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArg(final String arg){
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    {
        registry(new AboutCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskViewByNameCommand());
        registry(new TaskViewByIdCommand());
        registry(new TaskViewByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectCascadeRemoveByIdCommand());
        registry(new TaskBindByProjectIdCommand());
        registry(new TaskUnbindByProjectId());
        registry(new TasksShowByProjectIdCommand());
    }

    public boolean parseArgs(String[] args){
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(final String name) {
        if (name == null || name.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) return;
        command.execute();
    }

    private void registry(final AbstractCommand command){
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}
