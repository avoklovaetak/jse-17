package ru.volkova.tm.exception.empty;

import ru.volkova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }
}
