package ru.volkova.tm.command.project;

import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

}
