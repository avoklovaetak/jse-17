package ru.volkova.tm.command.project;

import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.model.Project;
import ru.volkova.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Override
    public String description() {
        return "show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
