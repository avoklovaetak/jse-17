package ru.volkova.tm.command.task;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "start task by index";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().startTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

}
