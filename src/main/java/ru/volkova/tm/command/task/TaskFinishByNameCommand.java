package ru.volkova.tm.command.task;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "finish task by name";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}
