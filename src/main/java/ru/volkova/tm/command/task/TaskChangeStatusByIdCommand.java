package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @Override
    public String description() {
        return "change task status by id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().changeTaskStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
