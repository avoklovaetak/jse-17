package ru.volkova.tm.command.task;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "finish task by id";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskById(id);
        if (task == null) throw new TaskNotFoundException();
    }

}