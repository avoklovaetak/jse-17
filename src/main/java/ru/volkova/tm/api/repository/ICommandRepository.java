package ru.volkova.tm.api.repository;

import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    void add(AbstractCommand command);

}
